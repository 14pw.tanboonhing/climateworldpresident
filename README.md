# CZ4001 VR Assignment: Climate World President by Team 6
## Trailer Video:
[Climate World President Trailer](https://youtu.be/xAOR3OnstII)

## Build:
[Climate World President](https://gitlab.com/14pw.tanboonhing/climateworldpresident/-/blob/ca9f0feba2b00ab2196a6f416020786ca253a086/release/Climate%20World%20President.exe)
<br> Filepath: release/Climate World President.exe

*or game scene can be found in Assets/Scenes/ClimateWorldPresidentScene.unity*

## Report: 
[CZ4001 VR Assignment Report](https://gitlab.com/14pw.tanboonhing/climateworldpresident/-/blob/master/CZ4001_VR%20Assignment%20Report_Team%206.pdf)


## Group members:
Tan Boon Hing, Tan Boon Ping, Ong Yu Hann, Lim Jun Hong, Steffi Ng Si Yu
