using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour
{
    public float selfDestructIn;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        selfDestructIn -= Time.deltaTime;
        if(selfDestructIn <=0)
        {
            Destroy(gameObject);
        }
    }
}
