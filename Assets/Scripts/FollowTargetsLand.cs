using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTargetsLand : MonoBehaviour
{
	public Transform[] target;
	public float speed;
	public float damping = 6.0f;
	public int current, prevTarget;
	public Animator anim;

	public float[] waitingTimes;
	private float currentWaitingTime;

	//private float prevAngleDiffference;

	void FixedUpdate()
	{
		// Calcuate the turning angle
		Vector3 direction = target[current].position - transform.position;
		float angleDifference = Vector3.Dot(direction.normalized, transform.forward.normalized);
		//Debug.Log("angleDifference " + angleDifference);
		if (Mathf.Abs(angleDifference) < 0.95f)
		{
			// Turn animal on the spot until in the correct direction
			var rotation = Quaternion.LookRotation(target[current].position - transform.position);
			transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
			transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, transform.eulerAngles.z);
		}
		else if (Vector3.Distance(target[current].position, transform.position) > 0.2f)
		{
			// Moves the object to target position after reaching a reasonable direction
			transform.position = Vector3.MoveTowards(transform.position, target[current].position, Time.deltaTime * speed);
            var rotation = Quaternion.LookRotation(target[current].position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, transform.eulerAngles.z);
        }
        else  // give animal some idle animation instead of constantly walking
		{
			// check if animal is required to wait at target
			if (waitingTimes[current] > 0 && currentWaitingTime < waitingTimes[current])
			{
				if (anim.GetBool("isWalking"))
				{
					anim.SetBool("isWalking", false);  // set not walking
				}
				currentWaitingTime += Time.deltaTime;  // gives amount time from previous frame
			}
			else  // animal continue walking
			{
				anim.SetBool("isWalking", true);
				currentWaitingTime = 0;
				// get the animal's current facing direction
				//vector3 prevdirection = target[current].position - transform.position;
				//prevanglediffference = vector3.dot(prevdirection.normalized, transform.forward.normalized);
				prevTarget = current;
				// go to next target
				current = (current + 1) % target.Length; // loop
			}
		}
	}
}
