using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class PlayerLeftJoyStickMovement : MonoBehaviour
{
    public SteamVR_Action_Vector2 input;
    public float speed = 1;
    Vector3 newCenter;
    float headHeight;

    CharacterController characterController;

    // Start is called before the first frame update
    void Start()
    {
        Valve.VR.OpenVR.Chaperone.ResetZeroPose(ETrackingUniverseOrigin.TrackingUniverseStanding);

        characterController = GetComponent<CharacterController>();
        PositionController();
    }

    // Update is called once per frame
    void Update()
    {
        PositionController();
        if (input.axis.magnitude > 0.1f)
        {
            Vector3 direction = Player.instance.hmdTransform.TransformDirection(new Vector3(input.axis.x, 0, input.axis.y));
            characterController.Move(speed * Time.deltaTime * Vector3.ProjectOnPlane(direction, Vector3.up) - new Vector3(0, 9.81f, 0) * Time.deltaTime);
        }

        //Vector3 direction = Player.instance.hmdTransform.TransformDirection(new Vector3(input.axis.x, 0, input.axis.y));
        //transform.position += speed * Time.deltaTime * Vector3.ProjectOnPlane(direction, Vector3.up);
    }

    private void PositionController()
    {
        //Gets the head in local playspace
        headHeight = Mathf.Clamp(Camera.main.transform.localPosition.y, 1, 2);
        characterController.height = headHeight;

        //Cut in half, add skin
        newCenter = Vector3.zero;
        newCenter.y = characterController.height * 0.5f;
        newCenter.y += characterController.skinWidth;

        //Lets move the capsule in local space as well
        newCenter.x = Camera.main.transform.localPosition.x;
        newCenter.z = Camera.main.transform.localPosition.z;

        //Apply
        characterController.center = newCenter;
    }
}
