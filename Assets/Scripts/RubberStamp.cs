using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RubberStamp : MonoBehaviour
{
    public GameObject decalPrefab;
    public Transform decalSpawnPoint;
    public Transform[] corners;
    public bool hasSpawned = false, isApproved;
    void Update()
    {
        Debug.DrawRay(transform.position, -transform.up/10, Color.green);
        for (int x = 0; x < corners.Length; ++x)
        {
            Debug.DrawRay(corners[x].position, -transform.up/10, Color.green);
        }
    }

    private void SpawnDecal(RaycastHit hitInfo , GameObject hitObj)
    {
        Debug.Log("Decal Spawned");
        bool isPaper = hitObj.CompareTag("Paper");
        

        GameObject decal = Instantiate(decalPrefab);
        decal.transform.position = hitInfo.point;
        decal.transform.forward = hitInfo.normal * -1f;
        decal.transform.eulerAngles = new Vector3(decal.transform.eulerAngles.x , decalSpawnPoint.eulerAngles.y, decalSpawnPoint.eulerAngles.z);
        
        decal.transform.SetParent(hitInfo.transform);

        if (isPaper)
        {
            decal.GetComponent<SelfDestruct>().enabled = false;
            Policy policyScript = hitObj.GetComponent<Policy>();
            if (isApproved)
            {
                policyScript.SetApproved();
            }
            else
            {
                policyScript.SetRejected();

            }
            policyScript.AddDecal(decal);

        }
        hasSpawned = true;
    }

    private void OnTriggerExit(Collider other)
    {
        hasSpawned = false;

    }
    private void OnTriggerStay(Collider other)
    {
        if(hasSpawned)
        {
            return;
        }
        RaycastHit hitInfo;
        
        if (Physics.Raycast(transform.position, -transform.up, out hitInfo, 0.1f))
        {
            GameObject hitObj = hitInfo.collider.gameObject;
            for (int x = 0; x < corners.Length; ++x)
            {
                RaycastHit hitInfo2;
                Physics.Raycast(corners[x].position, -corners[x].up, out hitInfo2, 0.1f);
                if (hitInfo2.collider == null)
                {
                    return;
                }
                else if (!GameObject.ReferenceEquals(hitObj, hitInfo2.collider.gameObject))
                {
                    return;
                }
            }
            SpawnDecal(hitInfo, hitObj);

        }
    }
}
