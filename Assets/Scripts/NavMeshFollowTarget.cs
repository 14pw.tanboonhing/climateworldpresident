using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshFollowTarget : MonoBehaviour
{
    [SerializeField] private Transform[] movePositionTransform;
    [SerializeField] private float[] waitingTimes;
    [SerializeField] private int current;
    [SerializeField] private float targetStoppingDistance;
    private NavMeshAgent navMeshAgent;
    public Animator anim;
    private float currentWaitingTime = 0;

    private void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        //Debug.Log(Vector3.Distance(transform.position, movePositionTransform[current].position));
        if (Vector3.Distance(transform.position, movePositionTransform[current].position) < targetStoppingDistance)
        {
            if (waitingTimes[current] > 0 && currentWaitingTime < waitingTimes[current])
            {
                if (anim.GetBool("isWalking"))
                {
                    anim.SetBool("isWalking", false);
                }
                currentWaitingTime += Time.deltaTime;  // gives amt time from prev frame
            }
            else
            {
                anim.SetBool("isWalking", true);
                currentWaitingTime = 0;
                current = (current + 1) % movePositionTransform.Length;
            }
        }
        else
        {
            navMeshAgent.destination = movePositionTransform[current].position;
        }
    }
}
