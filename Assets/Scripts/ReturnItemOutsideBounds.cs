using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnItemOutsideBounds : MonoBehaviour
{
    private bool isGrabbed, isWithinBounds, isReturning;
    public float returnDuration, returnDelay;
    private float timer,delayTimer;
    public GameObject smokeParticles, smokeParticlesDestroyPrefab;
    private Vector3 initialPosition;
    private Quaternion initialRotation;
    public Transform dissappearPosition;
    // Start is called before the first frame update
    void Start()
    {
        isWithinBounds = true;
        isGrabbed = false;
        timer = 0;
        initialPosition = transform.position;
        initialRotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if(isReturning)
        {
            delayTimer += Time.deltaTime;
            if (delayTimer > returnDelay)
            {
                delayTimer = 0;
                ReturnToPosition();
            }
        }
        else if(!isGrabbed && !isWithinBounds)
        {
            timer += Time.deltaTime;
            if(timer >returnDuration)
            {
                timer = 0;
                Disappear();
            }
        }
    }

    private void Disappear()
    {
        GameObject smokeDestroy = Instantiate(smokeParticlesDestroyPrefab);
        smokeDestroy.transform.position = transform.position;
        transform.position = dissappearPosition.position;
        isReturning = true;
    }

    private void ReturnToPosition()
    {
        transform.position = initialPosition;
        transform.rotation = initialRotation;
        smokeParticles.SetActive(true);
        isReturning = false;
    }

    public void SetIsGrabbed(bool grabbed)
    {
        isGrabbed = grabbed;
        timer = 0;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bounds")
        {
            isWithinBounds = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Bounds")
        {
            isWithinBounds = false;
            timer = 0;
        }
    }
}
