using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTargetsFly : MonoBehaviour
{
	public Transform[] target;
	public float speed;
	public float damping = 6.0f;
	public int current, prevTarget;

	void FixedUpdate()
	{
		if (Vector3.Distance(target[current].position, transform.position) > 0.2f)
		{
			// Moves animal to target position
			transform.position = Vector3.MoveTowards(transform.position, target[current].position, Time.deltaTime * speed);
			// Turns animal
			var rotation = Quaternion.LookRotation(target[current].position - transform.position);
			transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
			transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, transform.eulerAngles.z);
		}
		else current = (current + 1) % target.Length;  // Go to next target
	}

}
