using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class PortalTeleporter : MonoBehaviour
{

	public Transform player;
	public Transform reciever;
	Camera playerCamera;
	private bool playerIsOverlapping = false;
	public Material receiverSkyBox;
	private Skybox cameraSkyBox;
	private Player playerScript;
    private void Start()
    {
		playerCamera = Camera.main;
		cameraSkyBox = playerCamera.GetComponent<Skybox>();
		playerScript = player.gameObject.GetComponent<Player>();
	}

    // Update is called once per frame
    void Update()
	{
		if (playerIsOverlapping)
		{
			Vector3 portalToPlayer = playerCamera.transform.position - transform.position;
			float dotProduct = Vector3.Dot(transform.up, portalToPlayer);
			// If this is true: The player has moved across the portal
			if (dotProduct < 0f)
			{
				// Teleport him!
				float rotationDiff = -Quaternion.Angle(transform.rotation, reciever.rotation);
				rotationDiff += 180;
				player.Rotate(Vector3.up, rotationDiff);

				//Vector3 positionOffset = Quaternion.Euler(0f, rotationDiff, 0f) * portalToPlayer;
				//player.position = reciever.position + positionOffset;

				//Valve.VR.OpenVR.Chaperone.ResetZeroPose(Valve.VR.ETrackingUniverseOrigin.TrackingUniverseSeated);

				//playerCamera.gameObject.transform.position = player.transform.position;

				Vector3 GlobalCameraPosition = playerCamera.transform.position;  //get the global position of VRcamera
				Vector3 GlobalPlayerPosition = player.position;
				Vector3 GlobalOffsetCameraPlayer = new Vector3(GlobalCameraPosition.x - GlobalPlayerPosition.x, GlobalCameraPosition.y - GlobalPlayerPosition.y, GlobalCameraPosition.z - GlobalPlayerPosition.z);
				//player.position = new Vector3(reciever.transform.position.x - GlobalOffsetCameraPlayer.x, reciever.transform.position.y - GlobalOffsetCameraPlayer.y, reciever.transform.position.z - GlobalOffsetCameraPlayer.z);
				Vector3 teleportPosition = new Vector3(reciever.transform.position.x - GlobalOffsetCameraPlayer.x, reciever.transform.position.y - GlobalOffsetCameraPlayer.y, reciever.transform.position.z - GlobalOffsetCameraPlayer.z);

				Vector3 playerFeetOffset = playerScript.trackingOriginTransform.position - playerScript.feetPositionGuess;
				playerScript.trackingOriginTransform.position = new Vector3(reciever.position.x, reciever.position.y-1, reciever.position.z) + playerFeetOffset;

				cameraSkyBox.material = receiverSkyBox;

				playerIsOverlapping = false;
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player")
		{
			playerIsOverlapping = true;
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player")
		{
			playerIsOverlapping = false;
		}
	}
}
