using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolicyManager : MonoBehaviour
{
    [SerializeField]
    private Policy[] policies;
    private int[] policyStatus;
    [SerializeField]
    private bool[] approveIsCorrect;
    [SerializeField]
    private int passingScore;
    [SerializeField]
    private GameObject successObject,failObject;
    public Animator[] successPortalAnims;
    public Animator[] failPortalAnims;
    private bool failPortalHasRised, successPortalHasRised;

    // Start is called before the first frame update
    void Start()
    {
        failPortalHasRised = false;
        successPortalHasRised = false;
        for (int i=0;i<policies.Length;++i)
        {
            policies[i].setPolicyIndex(i);
        }
        policyStatus = new int[policies.Length];
        resetPoliciesStatus();
    }

    //status 0 means not completed, 1 means approved, 2 means rejected
    public void SetPolicyStatus(int index,int status)
    {
        policyStatus[index] = status;
        if(AreAllPoliciesCompleted())
        {
            if (GetUserPassed())
            {
                successObject.SetActive(true);
                failObject.SetActive(false);
                if (!successPortalHasRised)
                {
                    gameObject.GetComponent<PortalTextureSetup>().InitPortalTexture(0);
                    gameObject.GetComponent<PortalTextureSetup>().InitPortalTexture(3);
                    foreach (Animator anim in failPortalAnims)
                    {
                        anim.enabled = false;
                    }
                    successPortalHasRised = true;
                }
                
            }
            else
            {
                successObject.SetActive(false);
                failObject.SetActive(true);
                
                if (!failPortalHasRised)
                {
                    gameObject.GetComponent<PortalTextureSetup>().InitPortalTexture(1);
                    gameObject.GetComponent<PortalTextureSetup>().InitPortalTexture(2);
                    foreach (Animator anim in successPortalAnims)
                    {
                        anim.enabled = false;
                    }
                    failPortalHasRised = true;
                }
            }
        }
    }

    public bool GetUserPassed()
    {
        int score = 0;
        for (int i = 0; i < policyStatus.Length; ++i)
        {
            if ((policyStatus[i] == 1) == approveIsCorrect[i])
            {
                ++score;
            }
        }
        return score >= passingScore;
    }

    public bool AreAllPoliciesCompleted()
    {
        for (int i = 0; i < policyStatus.Length; ++i)
        {
            if(policyStatus[i] ==0)
            {
                return false;
            }
        }
        return true;
    }

    public void resetPoliciesStatus()
    {
        for (int i = 0; i < policyStatus.Length; ++i)
        {
            policyStatus[i] = 0;
        }
    }
}
