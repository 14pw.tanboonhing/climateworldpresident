using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using Valve.VR;

[System.Serializable]
public class SetOfPortals
{
	public Camera cameraA;
	public Camera cameraB;
	public MeshRenderer cameraMatA;
	public MeshRenderer cameraMatB;
	public bool hasBeenInit;
}

public class PortalTextureSetup : MonoBehaviour {

	public List<SetOfPortals> listOfSetOfPortals;

	public GameObject failObjects;

	private bool eyeFound = false;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < listOfSetOfPortals.Count; ++i)
        {
			if (listOfSetOfPortals[i].cameraA.targetTexture != null)
			{
				listOfSetOfPortals[i].cameraA.targetTexture.Release();
			}

			if (listOfSetOfPortals[i].cameraB.targetTexture != null)
			{
				listOfSetOfPortals[i].cameraB.targetTexture.Release();
			}
		}
	}

	public bool InitPortalTexture(int _portalIndex)
    {
		if (XRSettings.eyeTextureWidth > 0 && !listOfSetOfPortals[_portalIndex].hasBeenInit)
		{
			listOfSetOfPortals[_portalIndex].cameraB.targetTexture = new RenderTexture(XRSettings.eyeTextureWidth, XRSettings.eyeTextureHeight, 24);
			listOfSetOfPortals[_portalIndex].cameraMatA.material.mainTexture = listOfSetOfPortals[_portalIndex].cameraB.targetTexture;

			listOfSetOfPortals[_portalIndex].cameraA.targetTexture = new RenderTexture(XRSettings.eyeTextureWidth, XRSettings.eyeTextureHeight, 24);
			listOfSetOfPortals[_portalIndex].cameraMatB.material.mainTexture = listOfSetOfPortals[_portalIndex].cameraA.targetTexture;

			listOfSetOfPortals[_portalIndex].hasBeenInit = true;

			return true;
		}
		return false;
	}

    private void Update()
    {
		//if (eyeFound || !XRSettings.enabled || listOfSetOfPortals[0].hasBeenInit)
		//	return;

  //      if (XRSettings.eyeTextureWidth > 0)
  //      {
		//	InitPortalTexture(0);
		//}

		if (Input.GetKeyDown(KeyCode.Space))
        {
			Valve.VR.OpenVR.Chaperone.ResetZeroPose(ETrackingUniverseOrigin.TrackingUniverseSeated);
		}
    }

}
