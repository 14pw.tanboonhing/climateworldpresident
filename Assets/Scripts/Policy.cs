using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Policy : MonoBehaviour
{
    [SerializeField]
    private bool rejected, approved;
    private List<GameObject> decals;
    [SerializeField]
    private PolicyManager policyManager;
    [SerializeField]
    private GameObject smokeVFX;
    [SerializeField]
    private int policyIndex;
    // Start is called before the first frame update
    void Start()
    {
        decals = new List<GameObject>();
        InitBools();
    }
    public void setPolicyIndex(int index)
    {
        policyIndex = index;
    }
    public bool GetRejected()
    {
        return rejected;
    }
    public void SetRejected()
    {
        if(rejected)
        {
            return;
        }
        policyManager.SetPolicyStatus(policyIndex, 2);
        rejected = true;
        if (approved)
        {
            approved = false;
            DestroyAllDecals();
        }
    }

    public bool GetApproved()
    {
        return approved;
    }
    public void SetApproved()
    {
        if (approved)
        {
            return;
        }
        policyManager.SetPolicyStatus(policyIndex, 1);
        approved = true;
        if(rejected)
        {
            rejected = false;
            DestroyAllDecals();
        } 
    }
    public void InitBools()
    {
        rejected = false;
        approved = false;
    }

    public void AddDecal(GameObject decalGameObject)
    {
        decals.Add(decalGameObject);
        if(decals.Count >20)
        {
            GameObject toBeDestroyed = decals[0];
            decals.RemoveAt(0);
            Destroy(toBeDestroyed);
        }
    }
    public void DestroyAllDecals()
    {
        for (var i = 0; i < decals.Count; ++i)
        {
            Destroy(decals[i]);
        }
        decals.Clear();
        smokeVFX.SetActive(true);
    }
}
