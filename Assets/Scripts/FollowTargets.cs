using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTargets : MonoBehaviour
{
	public Transform[] target;
	public float speed;
	public float damping = 6.0f;
	private int current;
	public Rigidbody rb;
	public bool isMoving, touchingWater, grabbed;
	public Animator anim;
	public GameObject[] bones;
	void FixedUpdate()
	{
		if (!isMoving)
		{
			return;
		}
		if (Vector3.Distance(target[current].position, transform.position) > 0.2f)
		{
			// Moves animal to target position
			transform.position = Vector3.MoveTowards(transform.position, target[current].position, Time.deltaTime * speed);
			// Turns animal
			var rotation = Quaternion.LookRotation(target[current].position - transform.position);
			transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
			transform.eulerAngles = new Vector3(-90, transform.eulerAngles.y, transform.eulerAngles.z);
		}
		else current = (current + 1) % target.Length;  // go to next target
	}

	public void GrabEvent()  // when grabbed
	{
		grabbed = true;
		StopMoving();
	}

	public void GrabEndEvent()  // when released
	{
		grabbed = false;
		StartMoving();
	}

	public void StartMoving()  // start animation when released
	{
		if(!touchingWater)
        {
			return;  // don't move if haven't touch water
        }
		foreach (GameObject bone in bones)
		{
			bone.SetActive(false);  // disabled bones
		}
		anim.enabled = true;
		rb.useGravity = false;
		rb.velocity = Vector3.zero;
		isMoving = true;
	}

	public void StopMoving()  // stop animation when grabbed
	{
		foreach (GameObject bone in bones)
		{
			bone.SetActive(true);  // enable bones to create ragdoll "wobbly" effect
		}
		anim.enabled = false;
		rb.useGravity = true;
		isMoving = false;
	}
	void OnTriggerEnter(Collider other)  // to check if animal have touched water
	{
		if (other.tag == "Water")
		{
			touchingWater = true;
			if (!grabbed)
			{
				StartMoving();
			}
			
		}
	}

	void OnTriggerExit(Collider other)   // to check if animal have touched water
	{
		if (other.tag == "Water")
		{
			touchingWater = false;
			if (!grabbed)
			{
				StopMoving();
			}
		}
	}
}
