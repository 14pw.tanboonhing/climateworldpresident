using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmitRandomSound : MonoBehaviour
{
    [SerializeField]
    public List<AudioClip> audioClips;
    private AudioClip currentClip;
    private AudioSource source;
    [SerializeField]
    private float MinWaitBetweenPlays = 1f;
    [SerializeField]
    private float MaxWaitBetweenPlays = 5f;
    private float waitTimeCountdown = -1f;
 
    void Start()
    {
        source = GetComponent<AudioSource>();
    }
 
    void Update()
    {
        if (!source.isPlaying)
        {
            if (waitTimeCountdown < 0f)
            {
                currentClip = audioClips[Random.Range(0, audioClips.Count)];
                source.clip = currentClip;
                source.Play();
                waitTimeCountdown = Random.Range(MinWaitBetweenPlays, MaxWaitBetweenPlays);
            }
            else
            {
                waitTimeCountdown -= Time.deltaTime;
            }
        }
    }
}